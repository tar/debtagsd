# coding: utf8
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
import django_housekeeping as hk
from backend.housekeeping import MakeUnreviewedTags, Apriori
from autotag.housekeeping import Autotag
from debdata.utils import atomic_writer
import django.db
import os
import backend.models as bmodels
import logging

log = logging.getLogger(__name__)


STAGES = ["main", "checks", "exports"]


class UpdateTODOLists(hk.Task):
    """
    Recompute all package TODO lists
    """
    DEPENDS = [MakeUnreviewedTags, Autotag, Apriori]

    @django.db.transaction.atomic
    def run_checks(self, stage):
        from . import checks

        log.info("Computing new per-package TODO lists...")

        # Refresh check engine
        checks.engine.refresh()

        count_checks_created = 0
        count_checks_removed = 0

        # Whitelist of nontrivial packages, as a precache a name->pkg mapping
        pkgdb = {x.name: x for x in bmodels.Package.objects.filter(trivial=False)}

        # Get a string-based tag database, filtered with only nontrivial
        # packages
        tagdb = bmodels.tag_debtags_db()

        # Remove all existing todo list items
        count_checks_removed += bmodels.TodoEntry.objects.all().count()
        bmodels.TodoEntry.objects.all().delete()

        for name, tags in tagdb.db.items():
            pkg = pkgdb.get(name, None)
            if pkg is None: continue
            found = pkg.update_todolist(tags=tags, cleanup_needed=False)
            if found > 0:
                log.debug("%s: %d entries in TODO list", name, found)
            count_checks_created += found

        log.info("%d todo list entries removed, %d entries created, difference: %d",
                 count_checks_removed, count_checks_created,
                 count_checks_created - count_checks_removed)


class ExportJS(hk.Task):
    """
    Export checks
    """
    def run_exports(self, stage):
        from django.template.loader import get_template
        from checks import checks

        # Export the tag checks as a javascript expression
        tpl = get_template("exports/tagchecks.js")
        res = tpl.render({
            "checks": list(checks.CheckEngine.list()),
        })

        if os.path.isdir("../htdocs/js"):
            pathname = os.path.abspath("../htdocs/js/tagchecks.js")
        else:
            pathname = os.path.abspath("debtagslayout/static/js/tagchecks.js")

        with atomic_writer(pathname, "wt") as fd:
            fd.write(res)
