"""
Django settings for debtags project.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

from debtagsd.lib.log_utils import get_logging_config
import os
import os.path
from django.utils.translation import ugettext_lazy as _
from signon import providers

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# BASE_DIR is the default from newer djangos. PROJECT_DIR could still be used
# in developers settings, so it's kept here for a while
PROJECT_DIR = BASE_DIR

DATA_DIR = os.path.join(BASE_DIR, 'data')

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'thisisnotreallysecretweonlyuseitfortestingharrharr'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    # https://github.com/spanezz/django-housekeeping
    'django_housekeeping',
    'deblayout',
    'debtagslayout',
    'signon',
    'backend',
    'aptxapianindex',
    'autotag',
    'checks',
    'api',
    'reports',
    'exports',
    'editor',
    'search',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    # Needs to stay right after AuthenticationMiddleware
    'signon.middleware.SignonMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

AUTHENTICATION_BACKENDS = [
    'signon.middleware.SignonAuthBackend',
]

ROOT_URLCONF = 'debtagsd.urls'


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            "./", "templates"
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# Database configuration for development environments
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': f'{DATA_DIR}/db-used-for-development.sqlite',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

LOCALE_PATHS = (os.path.join(PROJECT_DIR, "locale"), )

LANGUAGES = (
    ('de', _('German')),
    ('en', _('English')),
    ('es', _('Spanish')),
    ('it', _('Italian')),
    ('fr', _('French')),
)

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'

# Enable using system assets
from django.conf.global_settings import STATICFILES_DIRS # noqa
STATICFILES_DIRS += [
    ("common", "/usr/share/javascript/"),
]


ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)
MANAGERS = ADMINS

AUTH_USER_MODEL = 'backend.User'

SITE_ID = 1

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)


# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'debtagsd.wsgi.application'

BACKEND_DATADIR = "data/"

SIGNON_PROVIDERS = [
    providers.DebssoProvider(name="debsso", label="sso.debian.org"),
]

SIGNON_VIEW_MIXIN = "debtagslayout.mixins.SignonMixin"

SIGNON_TEST_FIXTURE = "backend.unittest.SignonFixtureMixin"


# Try importing local settings from local_settings.py, if we can't, it's just
# fine, use defaults from this file
try:
    from .local_settings import *  # noqa
except ModuleNotFoundError:
    pass

# Log to file on top of the normal django logging
LOGGING = get_logging_config(f"{DATA_DIR}/logs/django.log")
