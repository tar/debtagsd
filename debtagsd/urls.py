from __future__ import annotations
from django.conf.urls import url, include
import django.conf.urls
from django.urls import path
from debtagslayout.views import DebtagsTemplateView
import debtagslayout.views
import reports.views as rviews
import django.contrib.auth.views as auth_views

django.conf.urls.handler403 = debtagslayout.views.Error403.as_view()
django.conf.urls.handler404 = debtagslayout.views.Error404.as_view()
django.conf.urls.handler500 = debtagslayout.views.Error500.as_view()

urlpatterns = [
    url(r'^robots.txt$', DebtagsTemplateView.as_view(template_name='robots.txt', content_type="text/plain"),
        name="root_robots_txt"),
    url(r'^$', DebtagsTemplateView.as_view(template_name='index.html'), name="root_index"),
    url(r'^license/$', DebtagsTemplateView.as_view(template_name='license.html'), name="root_license"),
    url(r'^doc/sso/$', DebtagsTemplateView.as_view(template_name='doc/sso.html'), name="root_doc_sso"),
    url(r'^accounts/login/$', auth_views.LoginView.as_view(template_name='registration/login.html')),
    url(r'^accounts/logout/$', auth_views.LogoutView.as_view(next_page='/')),
    url(r'^api/', include("api.urls")),
    url(r'^rep/', include("reports.urls")),
    url(r'^reports/', include("reports.urls")),
    url(r'^exports/', include("exports.urls")),
    url(r'^edit/', include("editor.urls")),
    url(r'^search/', include("search.urls")),
    url(r'^getting-started/$', DebtagsTemplateView.as_view(template_name="reports/getting-started.html"),
        name="getting_started"),
    url(r'^statistics/$', rviews.Stats.as_view(), name="statistics"),
    path('signon/', include("signon.urls")),
]
