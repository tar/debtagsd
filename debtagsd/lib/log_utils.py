# Copyright © 2020 Truelite S.r.l
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import logging
import logging.handlers
import sys


class GroupWritableTimedRotatingFileHandler(logging.handlers.TimedRotatingFileHandler):
    """
    TimedRotatingFileHandler that creates group writable files
    """
    def _open(self):
        orig_umask = os.umask(0o002)
        try:
            return super()._open()
        finally:
            os.umask(orig_umask)


# Register GroupWritableTimedRotatingFileHandler with the available logging
# handlers
logging.handlers.GroupWritableTimedRotatingFileHandler = GroupWritableTimedRotatingFileHandler


class RequireStderrTTY(logging.Filter):
    """
    logging.Filter that checks if stderr is a tty
    """
    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        try:
            self._isatty = os.isatty(sys.stderr.fileno())
        except OSError:
            self._isatty = False

    def filter(self, record):
        return self._isatty


def get_logging_config(logpath):
    """
    Return a logging configuration that logs to logpath, with log rotation.

    If debugging, and stderr is a tty, it logs also to stderr.

    Mailing to admins is still preserved.
    """
    return {
        'version': 1,
        # 'disable_existing_loggers': False,
        'filters': {
            'require_debug_false': {
                '()': 'django.utils.log.RequireDebugFalse',
            },
            'require_debug_true': {
                '()': 'django.utils.log.RequireDebugTrue',
            },
            'require_stderr_tty': {
                '()': 'debtagsd.lib.log_utils.RequireStderrTTY',
            },
        },
        'formatters': {
            'logfile': {
                'format': '[%(asctime)s %(module)s] %(levelname)s: %(message)s',
            },
        },
        'handlers': {
            'console': {
                'level': 'INFO',
                'filters': ['require_stderr_tty'],
                'class': 'logging.StreamHandler',
            },
            'null': {
                'class': 'logging.NullHandler',
            },
            'mail_admins': {
                'level': 'ERROR',
                'filters': ['require_debug_false'],
                'class': 'django.utils.log.AdminEmailHandler'
            },
            'file': {
                'level': 'DEBUG',
                'class': 'logging.handlers.GroupWritableTimedRotatingFileHandler',
                'filename': logpath,
                'when': 'midnight',
                'backupCount': 7,
                'filters': ['require_debug_false'],
                'formatter': 'logfile',
            },
        },
        'loggers': {
            'django': {
                'handlers': ['console'],
            },
            'django.request': {
                'handlers': ['file', 'mail_admins'],
                'level': 'INFO',
                'propagate': False,
            },
            'django.security': {
                'handlers': ['mail_admins'],
                'level': 'ERROR',
                'propagate': False,
            },
            'django.db.backends': {
                'level': 'INFO',
                'handlers': ['file'],
                'propagate': True,
            },
            'py.warnings': {
                'handlers': ['console'],
            },
        },
        'root': {
            'level': 'INFO',
            'handlers': ['file', 'console'],
        },
    }
