from __future__ import annotations
from django.utils.translation import ugettext as _
from django import http
from django.shortcuts import render
from django.views.generic import View
import json
from backend.utils import add_cors_headers
import backend.models as bmodels
from checks import checks


def stable_tags_view(request):
    """
    Export the stable tag database, as text
    """
    db = bmodels.tag_debtags_db()
    response = http.HttpResponse(content_type="text/plain")
    add_cors_headers(response)
    for pkg, tags in db.iter_packages_tags():
        if not tags:
            continue
        print("%s:" % pkg, ", ".join(sorted(tags)), file=response)
    return response


def vocabulary_js_view(request):
    """
    Export the tag vocabulary in javascript
    """
    facets = dict((f.name, (f.sdesc, f.ldesc_html)) for f in bmodels.Facet.objects.all())
    tags = dict((t.name, (t.sdesc, t.ldesc_html)) for t in bmodels.Tag.objects.all())

    response = render(request, "exports/vocabulary.js",
                      dict(
                          facets=json.dumps(facets, separators=(',', ':')),
                          tags=json.dumps(tags, separators=(',', ':')),
                      ))
    response["Content-Type"] = "application/javascript"
    add_cors_headers(response)
    return response


def tagchecks_js(request):
    """
    Export the tag checks as a javascript expression
    """
    response = render(request, "exports/tagchecks.js",
                      dict(
                          checks=list(checks.CheckEngine.list()),
                      ))
    response["Content-Type"] = "application/javascript"
    add_cors_headers(response)
    return response


class OverrideFormatter:
    def __init__(self):
        import textwrap
        self.wrapper = textwrap.TextWrapper(
            width=72,
            expand_tabs=False,
            replace_whitespace=False,
            drop_whitespace=True,
            initial_indent="",
            subsequent_indent=" ",
            fix_sentence_endings=False,
            break_long_words=False,
            break_on_hyphens=False,
        )

    def tags_to_overrides(self, pkg, tags):
        """
        Generate override field lines
        """
        first = True
        for line in self.wrapper.wrap(", ".join(sorted(tags))):
            if first:
                yield "{} Tag {}".format(pkg, line)
                first = False
            else:
                yield line


class Overrides(View):
    def get(self, request, name, *args, **kw):
        response = http.HttpResponse(content_type="text/plain")
        if name == "main":
            filename = "tags"
        else:
            filename = "tags.name"
        response['Content-Disposition'] = 'inline; filename=' + filename

        formatter = OverrideFormatter()
        db = bmodels.tag_debtags_db()

        for pkg in bmodels.Package.objects.filter(override_name=name):
            tags = db.tags_of_package(pkg.name)
            response.write("\n".join(formatter.tags_to_overrides(pkg.name, tags)))
            response.write(b"\n")

        return response
