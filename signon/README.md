# Design notes

## Handling of external signon services

This handles authentication against external providers, and mapping
authenticated identities to site users.

## `models.Identity`

Represents an identity in an external authentication providers.

`Identity.person` is a nullable foreign key, pointing to the site user that
logs in using this identity and provider. If it is set, the Identity is
considered *bound*, else it's *unbound*.

A user who signs in using an unbound Identity is still considered anonymous by
Django's authentication, and the site can define procedures for binding
identities to actual users.

## providers

Represent authentication providers and how to interact with them.


# Using signon

## Dependencies

```
apt install python3-jwcrypto python3-requests-oauthlib python3-gitlab
```

## Adapt the User model

Provide `get_user_model().objects.get_housekeeper()` to return a user object to
use to sign `Identity` audit logs.

If `SIGNON_AUTO_CREATE_USER` is set to True, provide
`get_user_model().objects.create_from_identity(identity: signon.models.Identity)`
to create user objects from a `signon.models.Identity` object.


## Define a view mixin

Create a mixin class to be used by signon views. The class should at least add
`base_template` to the template context, to point to the master template to
use. For example:

```py
class SignonMixin(DCLayoutMixin):
    """
    Mixin for signon views
    """
    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["base_template"] = "dc-base.html"
        return ctx
```

Set `SIGNON_VIEW_MIXIN` to the fully qualified class name for the mixin.


## Testing

Create a base fixture that:

* Defines `self.user1` and `self.user2` as two distinct users
* Has a `NamedObject` `self.identities` that stores identity objects for the
  tests

For example:

```py
class SignonFixtureMixin(BaseFixtureMixin):
    """
    Base test fixture for signon tests
    """
    @classmethod
    def setUpClass(cls):
        from signon.models import Identity
        super().setUpClass()
        cls.add_named_objects(
            identities=NamedObjects(Identity),
        )
        cls.create_person("user1", status=const.STATUS_DD_NU)
        cls.create_person("user2", status=const.STATUS_DD_NU)

        cls.user1 = cls.persons.user1
        cls.user2 = cls.persons.user2
```

Set `SIGNON_TEST_FIXTURE` in settings to point to that mixin, for example:

```py
SIGNON_TEST_FIXTURE = "backend.unittest.SignonFixtureMixin"
```

# Settings reference

## `SIGNON_PROVIDERS`

List of `signon.providers.Provider` objects that describe the active signon
providers for the site.


## `SIGNON_VIEW_MIXIN`

Base mixin class to add to `signon:*` views to integrate them in the site.


## `SIGNON_TEST_FIXTURE`

Base mixin class to add to `signon` test fixtures to integrate them in the site.


## `SIGNON_AUTO_CREATE_USER`

If `True`, logging in with an unbound identity and no other active bound
identities, will create a new user for it.


## `SIGNON_TEST_IDENTITIES`

`{str: dict}` mapping provider names to `Identity.objects.get(…)` kwargs, to
force login some identities in a devel setting.

This is ignored if `DEBUG` is `False`.


## `SIGNON_AUTO_BIND`

If `True`, logging in with an unbound identity while there are active bound
identities, automatically binds it to the current user.


## `SIGNON_DEFAULT_REDIRECT`

Default view name to redirect to on views like login, logout, or OIDC
callbacks.
