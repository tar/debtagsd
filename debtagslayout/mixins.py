from __future__ import annotations
from typing import Optional
from dataclasses import dataclass
from debtagsd.lib import assets


@dataclass
class NavLink:
    url: str
    label: str
    icon: Optional[str] = None


class DebtagsLayoutMixin(assets.AssetMixin):
    assets = [assets.Bootstrap4, assets.ForkAwesome]


class SignonMixin(DebtagsLayoutMixin):
    """
    Mixin for signon views
    """
    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["base_template"] = "debtags-base.html"
        return ctx
