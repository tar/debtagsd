from __future__ import annotations
from django.test import TestCase, override_settings
from django.urls import reverse
from backend.unittest import BackendDataMixin


@override_settings(BACKEND_DATADIR="testdata")
class TestView(BackendDataMixin, TestCase):
    def test_search(self):
        client = self.make_test_client()
        response = client.get(reverse("search_packages"))
        self.assertContains(response, "Debtags package search")
        self.assertNotContains(response, "Refine search")

        response = client.get(reverse("search_packages") + "?q=debian")
        self.assertContains(response, "Refine search")

        response = client.get(reverse("search_packages") + "?q=debian&qf=programs")
        self.assertContains(response, "Refine search")

        response = client.get(reverse("search_packages") + "?q=debian&wl=use::searching")
        self.assertContains(response, "Refine search")

        response = client.get(reverse("search_packages") + "?q=debian&qf=programs&wl=use::searching")
        self.assertContains(response, "Refine search")

        response = client.get(reverse("search_packages") + "?q=debian&qf=desktop")
        self.assertContains(response, "No results")

        response = client.get(reverse("search_packages") + "?q=debian&wl=x11::font")
        self.assertContains(response, "No results")

        response = client.get(reverse("search_packages") + "?q=debian&qf=programs&wl=x11::font")
        self.assertContains(response, "No results")

        response = client.get(reverse("search_packages") + "?q=debian&qf=desktop&wl=use::searching")
        self.assertContains(response, "No results")

        response = client.get(reverse("search_packages") + "?q=debian&qf=desktop&wl=x11::font")
        self.assertContains(response, "No results")
