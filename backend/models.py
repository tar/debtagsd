from __future__ import annotations
from django.utils.translation import ugettext_lazy as _
import django.db
from django.db import models
from django.urls import reverse
from django.conf import settings
from django.contrib.auth.models import BaseUserManager, PermissionsMixin
from django.utils import timezone
from debian import debtags
import os
import os.path
import re
import shutil
import json
import logging
import pickle
from debdata import utils
import debdata.patches

log = logging.getLogger(__name__)

patch_sequence = utils.Sequence()  # Sequence number used to generate patch names


class UserManager(BaseUserManager):
    def create_user(self, username, password=None, **other_fields):
        if not username:
            raise ValueError('Users must have an username')
        user = self.model(
            username=self.normalize_email(username),
            **other_fields
        )
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password, **other_fields):
        other_fields["is_superuser"] = True
        other_fields["is_staff"] = True
        return self.create_user(username, **other_fields)

    def get_housekeeper(self):
        """
        Return the housekeeping person, creating it if it does not exist yet
        """
        # Ensure that there is a __housekeeping__ user
        try:
            return self.get(username="debtagsd@debtags.debian.org")
        except self.model.DoesNotExist:
            return self.create_user(
                username="debtagsd@debtags.debian.org",
                is_staff=False,
                full_name="debtags.debian.org Housekeeping Robot")

    def create_from_identity(self, identity):
        if identity.issuer == "salsa":
            return self.create(
                    full_name=identity.fullname,
                    username=f"{identity.username}@salsa.debian.org")
        elif identity.issuer == "debsso":
            try:
                local_part, domain = identity.subject.split("@", 1)
            except ValueError:
                local_part = identity.subject
                domain = None

            if domain == "debian.org":
                username = f"{local_part}@debian.org"
            elif domain == "users.alioth.debian.org":
                username = f"{local_part}@users.alioth.debian.org"
            else:
                raise RuntimeError(f"Unsupported sso.debian.org domain {domain} for {identity.subject}")

            return self.create(full_name=identity.subject, username=username)
        else:
            raise NotImplementedError("Only salsa and debsso are supported for automatic user creation")


class User(PermissionsMixin, models.Model):
    objects = UserManager()

    # http://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address
    username = models.CharField(max_length=255, unique=True)
    full_name = models.CharField(max_length=255, blank=True)
    last_login = models.DateTimeField(_('last login'), default=timezone.now)
    is_staff = models.BooleanField(default=False)
    is_active = True
    # True if the person does not want any of their activity to be shown
    hidden = models.BooleanField(default=False)

    @property
    def is_dd(self):
        return self.username.endswith("@debian.org")

    def get_full_name(self):
        if self.full_name:
            return self.full_name
        return self.username

    def get_short_name(self):
        return self.username

    def __str__(self):
        return self.get_full_name()

    @property
    def unambiguous_user_id(self):
        try:
            local_part, domain = self.username.split("@", 1)
        except ValueError:
            return self.username
        if domain == "debian.org":
            return "{}@debian".format(local_part)
        elif domain == "users.alioth.debian.org":
            return "{}@alioth".format(local_part)
        else:
            return self.username

    # TODO
    # @models.permalink
    # def get_absolute_url(self):
    #     return ("contributor_detail", (), {"name": self.unambiguous_user_id})

    def get_username(self):
        return self.username

    @property
    def is_anonymous(self):
        return False

    @property
    def is_authenticated(self):
        return True

    def set_password(self, raw_password):
        pass

    def check_password(self, raw_password):
        return False

    def set_unusable_password(self):
        pass

    def has_usable_password(self):
        return False

    # def add_log(self, text):
    #     """
    #     Create a new log entry for this identifier
    #     """
    #     entry = UserLog(user=self, text=text)
    #     entry.save()
    #     return entry

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []


class Package(models.Model):
    """
    Information about one package
    """
    name = models.TextField(null=False, unique=True)
    version = models.TextField(null=False, unique=False)
    source = models.TextField(null=True, unique=False, db_index=True)
    sdesc = models.TextField()
    ldesc = models.TextField()
    popcon = models.IntegerField(default=0)
    ts_created = models.DateTimeField(auto_now_add=True)
    ts_updated = models.DateTimeField(auto_now=True)
    trivial = models.BooleanField(default=False, help_text=_("this package does not need manual tagging"))
    override_name = models.CharField(
            max_length=16, null=False, blank=True, default="",
            help_text=_("section name for exporting as override"))

    stable_tags = models.ManyToManyField('Tag', related_name='stable_packages')

    def __eq__(self, other):
        if other is None:
            return False
        return self.name.__eq__(other.name)

    # noqa See https://stackoverflow.com/questions/61212514/django-model-objects-became-not-hashable-after-upgrading-to-django-2-2
    # and https://code.djangoproject.com/ticket/30333
    __hash__ = models.Model.__hash__

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        if self.trivial:
            return reverse("report_pkginfo", args=[self.name])
        else:
            return reverse("editor_edit", args=[self.name])

    def to_dict(self, with_ldesc=True, with_stags=True, with_todolist=True, with_stamps=True):
        res = dict(
            name=self.name,
            sdesc=self.sdesc,
            popcon=self.popcon,
            source=self.source,
        )
        if with_ldesc:
            res["ldesc"] = self.ldesc
        if with_stags:
            res["stable_tags"] = [x.name for x in self.stable_tags.all()]
        if with_todolist:
            res["todolist"] = [x.to_dict() for x in self.todolist.all()]
        if with_stamps:
            res["ts_created"] = self.ts_created
            res["ts_updated"] = self.ts_updated
        return res

    @property
    def ldesc_html(self):
        return utils.HTMLDescriptionRenderer.format(self.ldesc)

    @classmethod
    def by_name(cls, name):
        try:
            return cls.objects.get(name=name)
        except cls.DoesNotExist:
            return None

    @classmethod
    def by_names(cls, names):
        # TODO: see if it's better with an IN query
        res = []
        for name in names:
            try:
                res.append(cls.objects.get(name=name))
            except cls.DoesNotExist:
                pass
        return res

    def apply_patch(self, added, removed):
        self.stable_tags.remove(*removed)
        self.stable_tags.add(*added)
        self.update_todolist()

    def update_todolist(self, tags=None, cleanup_needed=True):
        from checks import checks
        if cleanup_needed:
            self.todolist.all().delete()
        found = 0
        if tags is None:
            tags = frozenset(t.name for t in self.stable_tags.all())
        for c, data in checks.engine.run(tags):
            entry = TodoEntry(pkg=self, check_id=c.ID, data=json.dumps(data))
            entry.save()
            found += 1
        return found


class TodoEntry(models.Model):
    """
    TODO list entry for a package
    """
    pkg = models.ForeignKey(Package, related_name="todolist", on_delete=models.CASCADE)
    check_id = models.IntegerField()
    data = models.TextField()

    @classmethod
    def stats(cls):
        cursor = django.db.connection.cursor()
        cursor.execute("SELECT check_id, count(*) FROM backend_todoentry GROUP BY check_id")
        for check_id, count in cursor:
            yield check_id, count

    def to_dict(self):
        return dict(id=self.check_id, data=json.loads(self.data), msg=self.format())

    def format(self):
        from checks import checks
        checker = checks.CheckEngine.by_id(self.check_id)
        if checker is None:
            return "cannot find a formatter for id %d" % self.check_id
        else:
            return checker.format(self.pkg, json.loads(self.data))


class Facet(models.Model):
    """
    Information about one facet
    """
    name = models.TextField(null=False, unique=True)
    sdesc = models.TextField()
    ldesc = models.TextField()

    def __eq__(self, other):
        if other is None:
            return False
        return self.name.__eq__(other.name)

    # noqa See https://stackoverflow.com/questions/61212514/django-model-objects-became-not-hashable-after-upgrading-to-django-2-2
    # and https://code.djangoproject.com/ticket/30333
    __hash__ = models.Model.__hash__

    def __str__(self):
        return self.name

    @property
    def ldesc_html(self):
        return utils.HTMLDescriptionRenderer.format(self.ldesc)

    @classmethod
    def by_name(cls, name):
        try:
            return cls.objects.get(name=name)
        except cls.DoesNotExist:
            return None


class Tag(models.Model):
    """
    Information about one tag
    """
    name = models.TextField(null=False, unique=True)
    facet = models.ForeignKey(Facet, related_name="tags", on_delete=models.CASCADE)
    sdesc = models.TextField()
    ldesc = models.TextField()

    def __eq__(self, other):
        if other is None:
            return False
        return self.name.__eq__(other.name)

    # noqa See https://stackoverflow.com/questions/61212514/django-model-objects-became-not-hashable-after-upgrading-to-django-2-2
    # and https://code.djangoproject.com/ticket/30333
    __hash__ = models.Model.__hash__

    @property
    def tag_name(self):
        return self.name.split("::", 1)[1]

    @property
    def ldesc_html(self):
        return utils.HTMLDescriptionRenderer.format(self.ldesc)

    def __str__(self):
        return self.name

    def to_dict(self, with_ldesc=True):
        res = dict(
            name=self.name,
            sdesc=self.sdesc,
        )
        if with_ldesc:
            res["ldesc"] = self.ldesc
        return res

    @classmethod
    def by_name(cls, name):
        try:
            return cls.objects.get(name=name)
        except cls.DoesNotExist:
            return None

    @classmethod
    def by_names(cls, names):
        res = []
        for name in names:
            try:
                res.append(cls.objects.get(name=name))
            except cls.DoesNotExist:
                pass
        return res

    @classmethod
    def card_stats(cls):
        """
        Return a list of (tag, count packages) for every tag.

        Note: 'type' ends up straight into SQL, so make sure it is valid before
        passing it to this function.
        """
        cursor = django.db.connection.cursor()
        cursor.execute("""
            SELECT t.name, count(*)
              FROM backend_tag t
              JOIN backend_package_stable_tags pt ON pt.tag_id = t.id
             GROUP BY t.name
        """)
        for tname, count in cursor:
            yield tname, count


class Maintainer(models.Model):
    """
    Information about one maintainer
    """
    name = models.TextField()
    email = models.EmailField(max_length=254, null=False, unique=True)

    packages = models.ManyToManyField(Package, through='MaintPkg', related_name="maintainers")

    def __eq__(self, other):
        if other is None:
            return False
        return self.email.__eq__(other.email)

    # noqa See https://stackoverflow.com/questions/61212514/django-model-objects-became-not-hashable-after-upgrading-to-django-2-2
    # and https://code.djangoproject.com/ticket/30333
    __hash__ = models.Model.__hash__

    def __str__(self):
        return self.email

    @property
    def display_name(self):
        if self.name:
            return self.name
        else:
            return self.email

    @classmethod
    def by_email(cls, email):
        try:
            return cls.objects.get(email=email)
        except cls.DoesNotExist:
            return None


class MaintPkg(models.Model):
    """
    Association between a maintainer and a package
    """
    REL_TYPES = (
        ("M", "Maintainer"),
        ("U", "Uploader"),
        ("S", "Subscriber"),
    )
    maint = models.ForeignKey(Maintainer, on_delete=models.CASCADE)
    pkg = models.ForeignKey(Package, on_delete=models.CASCADE)
    rel_type = models.CharField(max_length=1, null=False, choices=REL_TYPES)

    class Meta:
        unique_together = ("maint", "pkg")


class AuditLog(models.Model):
    """
    Audit log for an operation performed by a user.

    For now, the only possible operation is a patch submission, and text always
    contains the formatted patchset.
    """
    user = models.ForeignKey(User, related_name="audit_log", on_delete=models.CASCADE)
    ts = models.DateTimeField(auto_now_add=True)
    text = models.TextField()


class Stats(models.Model):
    """
    Statistics collected at the end of maintenance"
    """
    ts = models.DateTimeField(auto_now_add=True)
    count_trivial = models.IntegerField(help_text=_("Count of trivially tagged packages"))
    count_robots = models.IntegerField(help_text=_("Count of packages tagged by robots"))
    count_humans = models.IntegerField(help_text=_("Count of packages tagged by humans"))
    count_tags = models.IntegerField(help_text=_("Number of tags in the vocabulary"))


class Patches(object):
    """
    Patch storage structure:

      patches/
        flat directory with all submitted patches
      patches-bytag/
        submitted patches dispatched in one directory for each submitter tag
      patches-bytag/patch
        merged patch of all the unreviewed changes in the directory
    """
    class AnonymousSubmissionChecker(object):
        def __init__(self):
            self.remarks = []
            self.new_patchset = debdata.patches.PatchSet()

        def check_patchset(self, pkg, patch):
            # Review the patch
            tags = set(x.name for x in pkg.stable_tags.all())
            added = patch.added - patch.removed
            removed = patch.removed - patch.added

            # Humans CANNOT edit trivial packages
            if pkg.trivial:
                added = set()
                removed = set()
                self.remarks.append("%s: ignoring attempt to edit trivial package" % pkg.name)

            # Humans CANNOT add special::* tags
            tmp = frozenset(x for x in added if x.startswith("special::"))
            if tmp:
                added = added - tmp
                self.remarks.append("%s: ignoring attempt to add special::* tags" % pkg.name)

            # Humans CANNOT remove any special::* tags except unreviewed
            tmp = frozenset(x for x in removed if x.startswith("special::") and x != "special::unreviewed")
            if tmp:
                removed = removed - tmp
                self.remarks.append("%s: ignoring attempt to remove special::* tags besides unreviewed" % pkg.name)

            # Humans CANNOT remove all tags from a package
            if not ((tags | added) - removed):
                self.remarks.append("%s: ignoring attempt to remove all tags from the package" % pkg.name)
                removed = frozenset()

            # Store in the edited patch
            if added or removed:
                self.new_patchset.add(pkg.name, added, removed)

            return added, removed

    # Lazy compilation of regexps
    @utils.lazy_property
    def re_cleantag(self):
        return re.compile(r'[^A-Za-z0-9]')

    def __init__(self, stable_tagdb=None, stable_tag_whitelist=None):
        if stable_tagdb:
            tagdb = stable_tagdb.copy()
        else:
            tagdb = None
        self._cached_stable_patched_tagdb = tagdb
        self._cached_stable_tag_whitelist = stable_tag_whitelist

    @property
    def cached_stable_patched_tagdb(self):
        """
        Get the stable tag DB, patched with patches in the approved patch queue
        """
        if self._cached_stable_patched_tagdb is None:
            tagdb = tag_debtags_db()
            self.patch_tagdb_with_approved_patches(tagdb)
            self._cached_stable_patched_tagdb = tagdb

        return self._cached_stable_patched_tagdb

    @property
    def cached_stable_tag_whitelist(self):
        """
        Get a tag whitelist with all tags that are accepted in the stable tagdb
        """
        if self._cached_stable_tag_whitelist is None:
            self._cached_stable_tag_whitelist = set(
                x.name for x in Tag.objects.exclude(name__startswith="special::").only("name"))
        return self._cached_stable_tag_whitelist

    def apply_patchset(self, patchset, checker=None):
        """
        Apply a PatchSet to the database

        Return the list of package objects that were touched by the patch
        """
        from checks import checks
        # Refresh check engine
        checks.engine.refresh()
        # Prefetch the tag objects from the database
        tagdb = {x.name: x for x in Tag.by_names(patchset.summary_tags)}

        res = []
        for pkg in Package.by_names(patchset.keys()):
            p = patchset[pkg.name]

            # Review the patch if we have a checker
            if checker is not None:
                added, removed = checker.check_patchset(pkg, p)
            else:
                added, removed = p.added, p.removed

            if not added and not removed:
                continue

            # Resolve tags to Tags
            pkg.apply_patch(
                added=(tagdb[t] for t in added if t in tagdb),
                removed=(tagdb[t] for t in removed if t in tagdb)
            )
            res.append(pkg)

        return res

    def apply_maint_patchset(self, task, patchset):
        """
        Apply a patchset generated by a maintenance task
        """
        self.apply_patchset(patchset)

    def apply_user_patchset(self, user, patchset):
        """
        Apply a PatchSet to the database, enforcing some constraints about
        anonymous contributions.

        Return the list of package objects that were touched by the patch, and
        a list of strings remarks on the patch if it has been altered due to
        consistency checks.
        """
        checker = Patches.AnonymousSubmissionChecker()
        pkgs = self.apply_patchset(patchset, checker=checker)
        AuditLog.objects.create(
            user=user,
            text=str(checker.new_patchset))
        return pkgs, checker


def tag_stats():
    pkgcount = Package.objects.count()
    count_untagged = Tag.by_name("special::unreviewed").stable_packages.count()
    return dict(
        count_packages=pkgcount,
        count_trivial=Package.objects.filter(trivial=True).count(),
        count_tagged=pkgcount - count_untagged,
        count_untagged=count_untagged,
        count_tags=Tag.objects.count(),
    )


# TODO: use django cache, and clear it when applying patches to the DB
def tag_debtags_db():
    "Return a debtags.DB object with the stable tag database"
    fwd_db = dict()
    bck_db = dict()
    for pkg, tag in Package.objects.values_list("name", "stable_tags__name"):
        if tag is None:
            continue
        if pkg in fwd_db:
            fwd_db[pkg].add(tag)
        else:
            fwd_db[pkg] = set((tag,))
        if tag in bck_db:
            bck_db[tag].add(pkg)
        else:
            bck_db[tag] = set((pkg,))

    res = debtags.DB()
    res.db = fwd_db
    res.rdb = bck_db
    return res


class PkgExtradir(object):
    def __init__(self, name):
        datadir = getattr(settings, "BACKEND_DATADIR", "data")
        pkgextradir = getattr(settings, "BACKEND_PKGEXTRADIR", os.path.join(datadir, "pkgs"))
        if name.startswith("lib"):
            self.pathname = os.path.join(pkgextradir, name[:4], name)
        else:
            self.pathname = os.path.join(pkgextradir, name[0], name)
        self.cache = dict()

    def _filename(self, key):
        # FIXME: key would need sanitizing so that it fits in a file. But as
        # long as this function is not exported to malicious sources, we don't
        # need the performance loss. Key is a simple string for legitimate
        # uses.
        return os.path.join(self.pathname, key)

    def __getitem__(self, key):
        """
        Load item from per-package dir, or from local cache
        """
        res = self.cache.get(key, None)
        if res is None:
            fname = self._filename(key)
            try:
                with open(fname, "r") as fd:
                    res = pickle.load(fd)
                self.cache[key] = res
            except IOError:
                return None
        return res

    def __setitem__(self, key, val):
        """
        Save the item in the cache and the file system
        """
        self.cache[key] = val
        fname = self._filename(key)
        if not os.path.isdir(self.pathname):
            os.makedirs(self.pathname, mode=0o775)
        with utils.atomic_writer(fname, sync=False) as fd:
            pickle.dump(val, fd, 2)

    def __delitem__(self, key):
        """
        Remove the item from the cache and the file system
        """
        self.cache.pop(key, None)
        try:
            os.unlink(self._filename(key))
        except OSError:
            pass

    @classmethod
    def remove(cls, name):
        datadir = getattr(settings, "BACKEND_DATADIR", "data")
        pkgextradir = getattr(settings, "BACKEND_PKGEXTRADIR", os.path.join(datadir, "pkgs"))
        try:
            shutil.rmtree(os.path.join(pkgextradir, name))
        except OSError:
            pass


def load_apriori_ruleset():
    """
    Return the apriori ruleset, or None if it is not available
    """
    datadir = getattr(settings, "BACKEND_DATADIR", "data")
    apriori_cache = getattr(settings, "BACKEND_APRIORI_CACHE", os.path.join(datadir, "apriori-cache"))
    rules = None
    if os.path.exists(apriori_cache):
        with open(apriori_cache, "rb") as fd:
            rules = pickle.load(fd)
    return rules
