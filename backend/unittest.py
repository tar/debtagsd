from __future__ import annotations
from django.test import override_settings, Client
from django.contrib.auth.backends import ModelBackend
from django.conf import settings
import debtagsd.lib.unittest
from debian import debtags
import backend.models as bmodels
import os
import os.path
import shutil
from contextlib import contextmanager
import collections
import logging


class NamedObjects(debtagsd.lib.unittest.NamedObjects):
    def create(self, _name, **kw):
        self._update_kwargs_with_defaults(_name, kw)
        self[_name] = o = self._model.objects.create(**kw)
        return o


class TestAuthenticationBackend(ModelBackend):
    pass


class UserFixtureMixin(debtagsd.lib.unittest.UserFixtureMixin):
    TEST_AUTH_BACKEND = "backend.unittest.TestAuthenticationBackend"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        if cls.TEST_AUTH_BACKEND not in settings.AUTHENTICATION_BACKENDS:
            settings.AUTHENTICATION_BACKENDS.append(cls.TEST_AUTH_BACKEND)
            cls._remove_test_auth_backend = True
        else:
            cls._remove_test_auth_backend = False

    @classmethod
    def tearDownClass(cls):
        if cls._remove_test_auth_backend:
            settings.AUTHENTICATION_BACKENDS.remove(cls.TEST_AUTH_BACKEND)
        super().tearDownClass()

    def make_test_client(self, person=None, signon_identities=()):
        """
        Instantiate a test client, logging in the given person.

        If person is None, visit anonymously. If person is None but
        signon_identities is set, activate the given identities
        """
        client = Client()
        for identity in signon_identities:
            identity = self.identities[identity]
            if identity.issuer == "salsa":
                session = client.session
                session["signon_identity_salsa"] = identity.pk
                session.save()
            elif identity.issuer == "debsso":
                client.defaults["SSL_CLIENT_S_DN_CN"] = identity.subject
            else:
                raise NotImplementedError(f"{identity.issuer} not supported as identity during testing")

        if isinstance(person, str):
            person = self.persons[person]
        if person is not None:
            client.force_login(person, backend=self.TEST_AUTH_BACKEND)
        client.visitor = person
        return client


# Code taken from python's unittest/case.py
_LoggingWatcher = collections.namedtuple("_LoggingWatcher",
                                         ["records", "output"])


# Code taken from python's unittest/case.py
class _CapturingHandler(logging.Handler):
    """
    A logging handler capturing all (raw and formatted) logging output.
    """

    def __init__(self):
        super().__init__()
        self.watcher = _LoggingWatcher([], [])

    def flush(self):
        pass

    def emit(self, record):
        self.watcher.records.append(record)
        msg = self.format(record)
        self.watcher.output.append(msg)


@contextmanager
def _capture_logs():
    # Code taken from python's unittest/case.py
    logger = logging.getLogger(None)
    formatter = logging.Formatter("%(levelname)s:%(name)s:%(message)s")
    handler = _CapturingHandler()
    handler.setFormatter(formatter)
    old_handlers = logger.handlers[:]
    old_level = logger.level
    old_propagate = logger.propagate
    logger.handlers = [handler]
    logger.setLevel(logging.INFO)
    logger.propagate = False

    try:
        yield handler.watcher
    finally:
        logger.handlers = old_handlers
        logger.propagate = old_propagate
        logger.setLevel(old_level)


class BackendDataMixin(UserFixtureMixin):
    @classmethod
    def build_test_db(cls, stages=("main",)):
        cls.user = cls.users.create("test", username="test")

        with override_settings(BACKEND_DATADIR="testdata", DEBTAGS_HOUSEKEEPING_LOCAL=True):
            # FIXME: this is run before every test method and is SLOW, considering
            # that all test methods are readonly. There seems to be no support at
            # the moment for loading readonly test data only once
            from django_housekeeping import Housekeeping
            hk = Housekeeping()
            hk.autodiscover()
            hk.init()
            with _capture_logs() as log:
                skip_apps = ("signon",)
                hk.run(run_filter=lambda name: (
                    any(f":{app}." not in name for app in skip_apps)
                    and any(f"{stage}:" in name for stage in stages)))
            for entry in log.output:
                if entry.startswith("ERROR"):
                    raise RuntimeError(f"test setup housekeeping error: {entry}")

        tag_objects = {x.name: x for x in bmodels.Tag.objects.all()}

        db = debtags.DB()
        with open("testdata/tags-stable", "r") as fd:
            db.read(fd)
        for pkg, tags in db.iter_packages_tags():
            pkg = bmodels.Package.objects.get(name=pkg)
            pkg.stable_tags.set([tag_objects.get(tag) for tag in tags])

    @classmethod
    def _clear_filesystem(cls):
        for name in ("pkgs"):
            pathname = os.path.join("testdata", name)
            if os.path.isdir(pathname):
                shutil.rmtree(pathname)

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls._clear_filesystem()
        cls.build_test_db()

    @classmethod
    def tearDownClass(cls):
        bmodels.User.objects.all().delete()
        bmodels.Package.objects.all().delete()
        bmodels.TodoEntry.objects.all().delete()
        bmodels.Facet.objects.all().delete()
        bmodels.Tag.objects.all().delete()
        bmodels.Maintainer.objects.all().delete()
        bmodels.MaintPkg.objects.all().delete()
        bmodels.AuditLog.objects.all().delete()
        bmodels.Stats.objects.all().delete()
        cls._clear_filesystem()
        super().tearDownClass()


class SignonFixtureMixin(UserFixtureMixin):
    """
    Base test fixture for signon tests
    """
    @classmethod
    def setUpClass(cls):
        from signon.models import Identity
        super().setUpClass()
        cls.add_named_objects(
            identities=NamedObjects(Identity),
        )
        cls.users.create("user1", username="user1@debian")
        cls.users.create("user2", username="user2@debian")

        cls.user1 = cls.users.user1
        cls.user2 = cls.users.user2
