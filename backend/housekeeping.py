from __future__ import annotations
import django_housekeeping as hk
import django.db
from django.conf import settings
from . import models as bmodels
from collections import defaultdict, namedtuple
from debian import deb822
import requests
import os
import os.path
import io
import time
import re
import subprocess
from debdata.utils import atomic_writer, run_cmd
import gzip
import logging

log = logging.getLogger(__name__)

STAGES = ["backup", "main", "exports", "stats"]


class BackupTags(hk.Task):
    """
    Extract tags from the DB and back them up to a file
    """
    def run_backup(self, stage):
        if not self.hk.outdir:
            log.info("outdir not set: skipping tags backup")
            return

        backupdir = self.hk.outdir.path("backup")

        tagdb = defaultdict(set)
        for pkg, tag in bmodels.Package.objects.values_list("name", "stable_tags__name"):
            if tag is None:
                continue
            tagdb[pkg].add(tag)

        log.info("Backing up tags to %s/tags...", backupdir)
        with atomic_writer(os.path.join(backupdir, "tags"), "wt") as fd:
            for pkg, tags in sorted(tagdb.items()):
                print("{}: {}".format(pkg, ", ".join(sorted(tags))), file=fd)


def download_if_old(url, local):
    if getattr(settings, "DEBTAGS_HOUSEKEEPING_LOCAL", False):
        if not os.path.exists(local):
            raise RuntimeError("{} does not exist locally and DEBTAGS_HOUSEKEEPING_LOCAL is True".format(local))
        return
    elif os.path.exists(local) and os.path.getmtime(local) > time.time() - 3600 * 6:
        return

    chunk_size = 4096

    log.info("Downloading %s to %s...", url, local)

    bundle = '/etc/ssl/ca-debian/ca-certificates.crt'
    if os.path.exists(bundle):
        res = requests.get(url, stream=True, verify=bundle)
    else:
        res = requests.get(url, stream=True)

    res.raise_for_status()

    with atomic_writer(local, "wb") as fd:
        for chunk in res.iter_content(chunk_size):
            fd.write(chunk)


class Popcon(hk.Task):
    """
    Popcon votes
    """
    NAME = "popcon"

    def __init__(self, *args, **kw):
        super(Popcon, self).__init__(*args, **kw)

        self.votes = {}

    def run_main(self, stage):
        datadir = getattr(settings, "BACKEND_DATADIR", "data")
        source = os.path.join(datadir, "popcon.gz")

        download_if_old("https://popcon.debian.org/by_vote.gz", source)

        log.info("Loading %s...", source)
        with gzip.open(source, "rb") as fd:
            for line in fd:
                # Skip comments
                if line.startswith(b'#'):
                    continue
                # Terminate before the totals
                if line.startswith(b'-'):
                    break
                try:
                    # See #833695 and #893418.
                    # FIXME: If we need to deal with by_vote containing
                    # arbitrary garbage, this is only a partial work-around.
                    # The parsing logic would need to be rewritten to allow
                    # spaces anywhere inside the package name.
                    line = line.decode("utf8")
                except UnicodeDecodeError:
                    pass
                # Split the line
                rank, name, inst, vote, rest = line.split(None, 4)
                # Store the mapping
                self.votes[name] = int(vote)


Pkg = namedtuple("Pkg", ("name", "ver", "src", "sec", "sdesc", "ldesc", "archs",
                         "predeps", "deps", "recs", "suggs", "enhs", "dist", "area"))


class BinPackages(hk.Task):
    """
    Binary package information
    """
    NAME = "binpackages"

    def __init__(self, *args, **kw):
        super(BinPackages, self).__init__(*args, **kw)

        self.by_name = {}
        self.by_section = {}

    def run_main(self, stage):
        from debian import deb822
        from debdata import utils

        datadir = getattr(settings, "BACKEND_DATADIR", "data")
        source = os.path.join(datadir, "all-merged.gz")
        download_if_old("https://debtags.debian.org/static/mergepackages/all-merged.gz", source)

        re_multivalue = re.compile(r'\s*,\s*')

        log.info("Loading %s...", source)
        with gzip.open(source, "rt", encoding="utf8") as fd:
            for pkg in deb822.Deb822.iter_paragraphs(fd):
                name = pkg["Package"]
                src = pkg.get("Source", name)
                if not src:
                    src = name

                section = pkg.get("Section", "unknown")
                splitsec = section.split("/", 1)
                if len(splitsec) == 1:
                    area = "main"
                    section = splitsec[0]
                else:
                    area, section = splitsec

                desc = pkg.get("Description", None)
                if desc is None:
                    continue
                sdesc, ldesc = utils.splitdesc(desc)

                def mv(name):
                    "Get list value for multivalue field"
                    return [x for x in re_multivalue.split(pkg.get(name, "")) if x]

                # Cook the source info and make a dict with what we need
                info = Pkg(name, pkg["Version"], src, section, sdesc, ldesc,
                           mv("Architecture"), mv("Pre-Depends"), mv("Depends"),
                           mv("Recommends"), mv("Suggests"), mv("Enhances"), mv("Distribution"), area)

                # Index it by various attributes
                self.by_name[name] = info
                self.by_section.setdefault(section, []).append(info)


Src = namedtuple("Src", ("name", "ver", "maint", "upls", "bd", "bdi"))


class SrcPackages(hk.Task):
    """
    Source package information
    """
    NAME = "srcpackages"

    def __init__(self, *args, **kw):
        super(SrcPackages, self).__init__(*args, **kw)
        self.sources = []

    def run_main(self, stage):
        from debian import deb822
        from email.utils import parseaddr, getaddresses

        datadir = getattr(settings, "BACKEND_DATADIR", "data")
        source = os.path.join(datadir, "all-merged-sources.gz")
        download_if_old("https://debtags.debian.org/static/mergepackages/all-merged-sources.gz", source)

        log.info("Loading %s...", source)
        re_multivalue = re.compile(r'\s*,\s*')
        with gzip.open(source, "rt", encoding="utf8") as fd:
            for src in deb822.Deb822.iter_paragraphs(fd):
                name = src["Package"]
                mname, memail = parseaddr(src.get("Maintainer", ""))

                uploaders = src.get("Uploaders", None)
                if uploaders is not None:
                    uploaders = getaddresses([uploaders])
                else:
                    uploaders = []

                def mv(name):
                    "Get list value for multivalue field"
                    return [x for x in re_multivalue.split(src.get(name, "")) if x]

                self.sources.append(
                    Src(name, src["Version"], (mname, memail), uploaders,
                        mv("Build-Depends"), mv("Build-Depends-Indep")))


Facet = namedtuple("Facet", ("name", "sdesc", "ldesc"))
Tag = namedtuple("Tag", ("name", "facet", "sdesc", "ldesc"))


class Vocabulary(hk.Task):
    """
    Vocabulary information
    """
    NAME = "vocabulary"

    def __init__(self, *args, **kw):
        super(Vocabulary, self).__init__(*args, **kw)
        self.facets = {}
        self.tags = {}

    def run_main(self, stage):
        from debian import deb822
        from debdata import utils

        datadir = getattr(settings, "BACKEND_DATADIR", "data")
        source = os.path.join(datadir, "vocabulary")
        sourcegit = os.path.join(datadir, "vocabulary.git")

        if not getattr(settings, "DEBTAGS_HOUSEKEEPING_LOCAL", False):
            if not os.path.isdir(sourcegit):
                run_cmd(
                    log,
                    ["git", "clone", "--bare",
                     "https://salsa.debian.org/debtags-team/debtags-vocabulary.git",
                     sourcegit])
            run_cmd(log, ["git", "--git-dir", sourcegit, "fetch"])

            vocdata = subprocess.check_output(
                    ["git", "--git-dir", sourcegit, "show", "master:debian-packages"], universal_newlines=False)
            with atomic_writer(source, "wb") as fd:
                fd.write(vocdata)

        log.info("Loading %s...", source)
        with open(source, "rt", encoding="utf8") as fd:
            for voc in deb822.Deb822.iter_paragraphs(fd):
                if "Facet" in voc:
                    name = voc["Facet"]
                    sdesc, ldesc = utils.splitdesc(voc.get("Description", None))
                    self.facets[name] = Facet(name, sdesc, ldesc)
                elif "Tag" in voc:
                    name = voc["Tag"]
                    if name.find("::") == -1:
                        # Skip legacy tags
                        continue
                    facet = self.facets[name.split("::")[0]]
                    sdesc, ldesc = utils.splitdesc(voc.get("Description", None))
                    self.tags[name] = Tag(name, facet, sdesc, ldesc)
                else:
                    log.warning("Found a record in vocabulary that is neither a Facet nor a Tag")

        log.info("%d facets, %d tags found in vocabulary", len(self.facets), len(self.tags))


class UpdateBinaries(hk.Task):
    """
    Update binary package information
    """
    NAME = "update_binaries"

    DEPENDS = [BinPackages, Popcon]

    def __init__(self, *args, **kw):
        super(UpdateBinaries, self).__init__(*args, **kw)
        self.wanted_srcs = set()
        self.pkgs = {}

    @django.db.transaction.atomic
    def run_main(self, stage):
        log.info("Updating package information...")
        pkgs = {}

        # Preload existing package info
        for pkg in bmodels.Package.objects.all():
            pkgs[pkg.name] = pkg

        to_delete = set(pkgs.keys())

        popcondb = self.hk.popcon.votes

        count_packages_created = 0
        count_packages_updated = 0
        count_packages_deleted = 0

        re_binnmu = re.compile(r"(.+)\+b\d+$")

        def add_want_src(pkg):
            # Remove binnmu suffix to match version in source package
            mo = re_binnmu.match(pkg.version)
            if mo:
                ver = mo.group(1)
            else:
                ver = pkg.version
            self.wanted_srcs.add((pkg.source, ver))

        # Read package descriptions
        for name, pkg in self.hk.binpackages.by_name.items():
            popcon = popcondb.get(name, 0)

            trivial = pkg.sec in ("debug", "libs", "oldlibs")

            version_changed = False

            override_name = pkg.area

            # Does it already exist?
            obj = pkgs.get(name, None)
            if obj is None:
                # No: create it
                obj = bmodels.Package.objects.create(
                        name=name, version=pkg.ver, sdesc=pkg.sdesc,
                        ldesc=pkg.ldesc, source=pkg.src, popcon=popcon,
                        trivial=trivial, override_name=override_name)
                pkgs[name] = obj
                version_changed = True
                log.info("Created package entry for %s", name)
                count_packages_created += 1
            else:
                version_changed = pkg.ver != obj.version or trivial != obj.trivial
                override_name_changed = override_name != obj.override_name
                # Prevent useless update traffic by triggering a popcon update
                # only if the vote has changed more than 5%. 0.01 is added to
                # prevent a division by zero.
                popcondiff = abs(obj.popcon-popcon)
                popconchange = popcondiff/(obj.popcon+0.01)
                if version_changed or (popcondiff > 3 and popconchange > 0.10) or override_name_changed:
                    # Update its information if needed
                    log.info("Updated package entry for %s: %s -> %s, popcon %d -> %d (%.1f%%)",
                             name, obj.version, pkg.ver, obj.popcon, popcon, popconchange * 100)
                    obj.version = pkg.ver
                    obj.sdesc = pkg.sdesc
                    obj.ldesc = pkg.ldesc
                    obj.source = pkg.src
                    obj.popcon = popcon
                    obj.trivial = trivial
                    obj.override_name = override_name
                    obj.save()
                    count_packages_updated += 1

            if version_changed:
                # Mark that we want a source update
                add_want_src(obj)
                # Update bin info in per-package extra data dir
                d = bmodels.PkgExtradir(name)
                d["bininfo"] = pkg

            # Mark that this is a package that we keep
            to_delete.discard(name)

        # Delete obsolete package entries from the DB
        for name in to_delete:
            log.info("Deleted obsolete package %s", name)
            pkg = pkgs.pop(name)
            pkg.delete()
            count_packages_deleted += 1
            bmodels.PkgExtradir.remove(name)

        # Keep the packages database we have just built as a precached package database
        self.pkgs = pkgs

        log.info("%d packages added, %d packages updated, %d packages removed",
                 count_packages_created, count_packages_updated, count_packages_deleted)


class UpdateSources(hk.Task):
    """
    Update maintainer information
    """
    DEPENDS = [UpdateBinaries, SrcPackages]

    @django.db.transaction.atomic
    def run_main(self, stage):
        log.info("Updating source package information...")

        log.info("Prefetching maintainers...")
        maint_cache = {}
        for m in bmodels.Maintainer.objects.all():
            maint_cache[m.email] = m

        def get_maint(name, email):
            m = maint_cache.get(email, None)
            if m is None:
                m, created = bmodels.Maintainer.objects.get_or_create(email=email, defaults=dict(name=name))
                maint_cache[email] = m
            elif m.name != name:
                m.name = name
                m.save()
            return m

        for src in self.hk.srcpackages.sources:
            if (src.name, src.ver) not in self.hk.update_binaries.wanted_srcs:
                continue

            # Build maintainer and uploader lists
            maint = get_maint(*src.maint)
            uploaders = []
            for uname, uemail in src.upls:
                u = get_maint(uname, uemail)
                if u is None:
                    continue
                if u == maint:
                    log.debug("Uploader same as maintainer: skipping %s for %s", uemail, src.name)
                    continue
                uploaders.append(u)

            # Update info in all binary packages
            for pkg in bmodels.Package.objects.filter(source=src.name):
                # Drop existing maintainers
                pkg.maintainers.clear()

                if maint is not None:
                    mp = bmodels.MaintPkg(maint=maint, pkg=pkg, rel_type='M')
                    mp.save()

                for m in uploaders:
                    mp = bmodels.MaintPkg(maint=m, pkg=pkg, rel_type='U')
                    mp.save()

                # Update bin info in per-package extra data dir
                d = bmodels.PkgExtradir(pkg.name)
                d["srcinfo"] = src


class UpdateVocabulary(hk.Task):
    """
    Update debtags tag vocabulary
    """
    DEPENDS = [Vocabulary]

    @django.db.transaction.atomic
    def run_main(self, stage):
        log.info("Updating source package information...")

        count_facets_created = 0
        count_facets_updated = 0
        count_facets_deleted = 0
        count_tags_created = 0
        count_tags_updated = 0
        count_tags_deleted = 0

        # Update facets
        facets = dict()
        for name, f in self.hk.vocabulary.facets.items():
            obj, created = bmodels.Facet.objects.get_or_create(
                name=name,
                defaults=dict(sdesc=f.sdesc, ldesc=f.ldesc))
            if created:
                log.info("Created facet entry for %s", name)
                count_facets_created += 1
            elif f.sdesc != obj.sdesc or f.ldesc != obj.ldesc:
                obj.sdesc = f.sdesc
                obj.ldesc = f.ldesc
                obj.save()
                log.info("Updated facet entry for %s", name)
                count_facets_updated += 1
            facets[name] = obj

        # Delete obsolete facets
        for facet in bmodels.Facet.objects.all().only("name"):
            if facet.name not in facets:
                log.info("Deleting obsolete facet %s", facet.name)
                facet.delete()
                count_facets_deleted += 1

        # Update tags
        for name, t in self.hk.vocabulary.tags.items():
            obj, created = bmodels.Tag.objects.get_or_create(
                name=name,
                defaults=dict(sdesc=t.sdesc, ldesc=t.ldesc, facet=facets[t.facet.name]))
            if created:
                log.info("Created tag entry for %s", name)
                count_tags_created += 1
            elif t.sdesc != obj.sdesc or t.ldesc != obj.ldesc:
                obj.sdesc = t.sdesc
                obj.ldesc = t.ldesc
                obj.save()
                log.info("Updated tag entry for %s", name)
                count_tags_updated += 1

        # Delete obsolete tags
        for tag in bmodels.Tag.objects.all().only("name"):
            if tag.name not in self.hk.vocabulary.tags:
                log.info("Deleting obsolete tag %s", tag.name)
                tag.delete()
                count_tags_deleted += 1

        log.info("%d facets added, %d facets updated, %d facets removed",
                 count_facets_created, count_facets_updated, count_facets_deleted)
        log.info("%d tags added, %d tags updated, %d tags removed",
                 count_tags_created, count_tags_updated, count_tags_deleted)


class TagTrivial(hk.Task):
    """
    Automatically manage tags for trivial packages
    """
    DEPENDS = [BinPackages, UpdateBinaries, UpdateVocabulary]

    @django.db.transaction.atomic
    def run_main(self, stage):
        log.info("Computing tag changes for trivial packages...")

        count_trivial_debug = 0
        count_trivial_libs = 0
        count_trivial_oldlibs = 0

        tag_deb = bmodels.Tag.objects.get(name="role::debug-symbols")
        tag_shlib = bmodels.Tag.objects.get(name="role::shared-lib")

        for pkg in bmodels.Package.objects.filter(trivial=True):
            info = self.hk.binpackages.by_name.get(pkg.name, None)
            if info is None:
                log.warn("no binpackages info found for trivial package %s", pkg.name)
                continue
            if info.sec == "debug":
                pkg.stable_tags.set([tag_deb])
                count_trivial_debug += 1
            elif info.sec == "libs":
                pkg.stable_tags.set([tag_shlib])
                count_trivial_libs += 1
            elif info.sec == "oldlibs":
                pkg.stable_tags.set([tag_shlib])
                count_trivial_oldlibs += 1

        log.info("trivial packages tagged: %d debug %d libs %d oldlibs",
                 count_trivial_debug, count_trivial_libs,
                 count_trivial_oldlibs)


class MakeUnreviewedTags(hk.Task):
    """
    Add special::unreviewed to packages without tags
    """
    DEPENDS = [TagTrivial, UpdateVocabulary]

    @django.db.transaction.atomic
    def run_main(self, stage):
        log.info("Adding missing special::unreviewed tags...")

        count_unreviewed_tags_added = 0
        unreviewed = bmodels.Tag.objects.get(name="special::unreviewed")

        for pkg in bmodels.Package.objects.filter(trivial=False, stable_tags=None):
            pkg.stable_tags.add(unreviewed)
            count_unreviewed_tags_added += 1

        log.info("%d special::unreviewed tags added", count_unreviewed_tags_added)


class Apriori(hk.Task):
    """
    Compute tagging rules from the stable tagset
    """
    NAME = "apriori"
    DEPENDS = [BinPackages, TagTrivial, MakeUnreviewedTags, UpdateVocabulary]

    def __init__(self, *args, **kw):
        super(Apriori, self).__init__(*args, **kw)
        self.facet_rules = []
        self.tag_rules = []
        self.neg_facet_rules = []
        self.neg_tag_rules = []

    @django.db.transaction.atomic
    def run_main(self, stage):
        from debian import debtags
        import debdata.apriori
        from debdata import utils
        import pickle
        log.info("Computing apriori tagging tips...")

        db = bmodels.tag_debtags_db()

        whitelist = frozenset(bmodels.Package.objects.filter(trivial=False).values_list("name", flat=True))

        fdb = debtags.DB()
        tdb = debtags.DB()
        for pkg, tags in db.iter_packages_tags():
            if pkg not in whitelist:
                continue
            tags = {t for t in tags if not t.startswith("special::")}
            facets = set(x.split("::")[0] for x in tags)
            fdb.insert(pkg, facets)
            tdb.insert(pkg, tags)

        # Configure the computation engine
        apriori = debdata.apriori.Apriori(quiet=True)

        if not os.path.exists(apriori.conf_apriori):
            log.warn("%s is missing: skipping update of apriori rule cache")
            return

        def postprocess(ruleset):
            ruleset.sort(key=lambda x: (x.tgt, x.conf))

        log.info("Computing facet correlations...")
        facet_rules = []
        for a in apriori.run_apriori(fdb):
            facet_rules.append(a)
        postprocess(facet_rules)

        log.info("Computing tag correlations...")
        tag_rules = []
        for a in apriori.run_apriori(tdb):
            tag_rules.append(a)
        postprocess(tag_rules)

        apriori.pick_defaults(reverse=True)

        log.info("Computing facet negative correlations...")
        neg_facet_rules = []
        for a in apriori.run_apriori(fdb):
            neg_facet_rules.append(a)
        postprocess(neg_facet_rules)

        log.info("Computing tag negative correlations...")
        neg_tag_rules = []
        for a in apriori.run_apriori(tdb):
            neg_tag_rules.append(a)
        postprocess(neg_tag_rules)

        log.info("Writing out cache...")
        datadir = getattr(settings, "BACKEND_DATADIR", "data")
        apriori_cache = getattr(settings, "BACKEND_APRIORI_CACHE", os.path.join(datadir, "apriori-cache"))
        with utils.atomic_writer(apriori_cache) as fd:
            pickle.dump(
                dict(f=facet_rules, t=tag_rules, nf=neg_facet_rules, nt=neg_tag_rules),
                fd, 2)

        self.facet_rules = facet_rules
        self.tag_rules = tag_rules
        self.neg_facet_rules = neg_facet_rules
        self.neg_tag_rules = neg_tag_rules


class ExportTags(hk.Task):
    """
    Export tags
    """
    def run_exports(self, stage):
        datadir = getattr(settings, "BACKEND_DATADIR", "data")
        target = os.path.join(datadir, "tags-stable")
        db = bmodels.tag_debtags_db()
        with atomic_writer(target, "wt") as fd:
            for pkg, tags in db.iter_packages_tags():
                if not tags:
                    continue
                print("%s:" % pkg, ", ".join(sorted(tags)), file=fd)


class DebianContributors(hk.Task):
    """
    Report contributions to Debian Contributors site
    """
    def run_exports(self, stage):
        from django.db.models import Min, Max
        from debiancontributors import Submission, Identifier

        auth_token = getattr(settings, "DC_AUTH_TOKEN", None)
        if auth_token is None:
            log.warn("no DC_AUTH_TOKEN configured, skipping contributors.debian.org harvesting")
            return

        sub = Submission("debtags.debian.org", auth_token=auth_token)
        for v in bmodels.AuditLog.objects.values("user__username").annotate(begin=Min("ts"), until=Max("ts")):
            sub.add_contribution_data(
                    Identifier(type="email", id=v["user__username"]),
                    type="tagger",
                    begin=v["begin"].date(),
                    end=v["until"].date())

        res, info = sub.post()
        if not res:
            log.error("contributors.debian.org submission failed: %r", info)


class OverrideFormatter(object):
    def __init__(self):
        import textwrap
        self.wrapper = textwrap.TextWrapper(
            width=72,
            expand_tabs=False,
            replace_whitespace=False,
            drop_whitespace=True,
            initial_indent="",
            subsequent_indent=" ",
            fix_sentence_endings=False,
            break_long_words=False,
            break_on_hyphens=False,
        )

    def tags_to_overrides(self, pkg, tags):
        """
        Generate override field lines
        """
        first = True
        for line in self.wrapper.wrap(", ".join(sorted(tags))):
            if first:
                yield "{} Tag {}".format(pkg, line)
                first = False
            else:
                yield line


class Overrides(hk.Task):
    """
    Export an overrides tarball for ftp-master
    """
    def load_package_names(self):
        """
        Generate (section, name) couples for all packages currently in Debian
        """
        url = "https://debtags.debian.org/static/mergepackages/debian-merged.gz"
        log.info("loading package information from %s", url)
        bundle = '/etc/ssl/ca-debian/ca-certificates.crt'
        if os.path.exists(bundle):
            res = requests.get(url, verify=bundle)
        else:
            res = requests.get(url)
        res.raise_for_status()
        buf = io.BytesIO(res.content)
        with gzip.open(buf, "rt", encoding="utf8") as fd:
            for pkg in deb822.Deb822.iter_paragraphs(fd):
                name = pkg["Package"]

                # Update sections
                sec = pkg["Section"].split("/")
                if len(sec) == 1:
                    sec = "main"
                else:
                    sec = sec[0]

                yield name, sec

    def load_packages_tags(self):
        """
        Generate (pkgname, section, {{tags}}) triples
        """
        tagdb = defaultdict(set)
        for pkg, tag in bmodels.Package.objects.values_list("name", "stable_tags__name"):
            if tag is None:
                continue
            if tag.startswith("special::"):
                continue
            tagdb[pkg].add(tag)

        for name, sec in self.load_package_names():
            tags = tagdb.get(name, None)
            if tags is None:
                continue
            yield name, sec, tags

    def make_overrides(self, pathname, rootpath):
        import tarfile

        # Sort data by section
        by_section = {
            "main": io.BytesIO(),
            "contrib": io.BytesIO(),
            "non-free": io.BytesIO(),
        }
        counts = {
            "main": 0,
            "contrib": 0,
            "non-free": 0,
        }

        formatter = OverrideFormatter()

        for pkg, sec, tags in self.load_packages_tags():
            buf = by_section[sec]
            buf.write(b"\n".join(line.encode("utf-8") for line in formatter.tags_to_overrides(pkg, tags)))
            buf.write(b"\n")
            counts[sec] += 1

        # Output it in a tar file
        with tarfile.open(pathname, "w:gz") as outtar:
            def add_buf(name, buf):
                tarinfo = tarfile.TarInfo(os.path.join(rootpath, name))
                tarinfo.size = len(buf.getvalue())
                tarinfo.mtime = time.time()
                tarinfo.mode = 0o644
                tarinfo.type = tarfile.REGTYPE
                tarinfo.uid = 0
                tarinfo.gid = 0
                tarinfo.uname = "root"
                tarinfo.gname = "root"
                buf.seek(0)
                outtar.addfile(tarinfo, buf)

            try:
                add_buf("tag", by_section["main"])
                add_buf("tag.contrib", by_section["contrib"])
                add_buf("tag.non-free", by_section["non-free"])
            except Exception:
                os.unlink(pathname)
                raise

        log.info("%s: %d main, %d contrib, %d non-free",
                 pathname, counts["main"], counts["contrib"], counts["non-free"])

    def run_exports(self, stage):
        static_root = getattr(settings, "STATIC_ROOT", None)
        if static_root is None:
            return

        import time
        version = time.strftime("%Y%m%d%H%M")
        rootdirname = "tags-" + version
        tar_basename = "tag-overrides.tar.gz"
        tarname = os.path.join("data", tar_basename)
        self.make_overrides(tarname, rootdirname)
        os.rename(tarname, os.path.join(static_root, tar_basename))


class SaveStats(hk.Task):
    """
    Save statistics in the database
    """
    def run_stats(self, stage):
        count_all = bmodels.Package.objects.count()
        count_trivial = bmodels.Package.objects.filter(trivial=True).count()
        count_robots = bmodels.Tag.objects.get(name="special::unreviewed").stable_packages.count()
        count_humans = count_all - count_trivial - count_robots
        count_tags = bmodels.Tag.objects.count()
        bmodels.Stats.objects.create(
            count_trivial=count_trivial,
            count_robots=count_robots,
            count_humans=count_humans,
            count_tags=count_tags,
        )
        log.info(
            "saved statistics: %d trivial packages,"
            " %d maintained by robots, %d maintained by humans, %d tags in vocabulary",
            count_trivial, count_robots, count_humans, count_tags)
