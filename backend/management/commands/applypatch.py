# debtagsd website backend
#
# Copyright (C) 2011  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.management.base import BaseCommand, CommandError
import django.db
from django.conf import settings
import optparse
import sys
import logging
from debdata import patches
from backend import models as bmodels

log = logging.getLogger(__name__)

class Command(BaseCommand):
    help = 'Apply a patch to the unstable tag DB'
    option_list = BaseCommand.option_list + (
        optparse.make_option("--tag", action="store", dest="tag", default="applypatch", metavar="string",
                             help="Tag to use for this package (default: %default)"),
        optparse.make_option("--quiet", action="store_true", dest="quiet", default=None, help="Disable progress reporting"),
    )

    def handle(self, *fnames, **opts):
        FORMAT = "%(asctime)-15s %(levelname)s %(message)s"
        if opts["quiet"]:
            logging.basicConfig(level=logging.WARNING, stream=sys.stderr, format=FORMAT)
        else:
            logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)

        tag = opts["tag"]

        db = bmodels.Patches()
        for fname in fnames:
            log.info("Applying patch %s", fname)
            # Read the patch
            ps = patches.PatchSet(fname=fname)
            # Apply it
            db.apply_patchset(tag, ps)

        log.info("%d patch(es) applied", len(fnames))
