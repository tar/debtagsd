from __future__ import annotations
from django import http
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404
from django.views.generic import View, TemplateView
import json
import datetime
from debtagslayout.mixins import DebtagsLayoutMixin
from checks import checks
from backend import models as bmodels
from collections import defaultdict
import calendar


class Stats(DebtagsLayoutMixin, TemplateView):
    template_name = "reports/stats.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)

        from django.db.models import Max
        from django.db import connection
        truncate_date = connection.ops.date_trunc_sql('month', 'ts')
        history = []
        for s in bmodels.Stats.objects.extra(
                {"month": truncate_date}).values("month").annotate(
                        trivial=Max("count_trivial"), robots=Max("count_robots"),
                        humans=Max("count_humans"), tags=Max("count_tags")).order_by("month"):
            if isinstance(s["month"], datetime.datetime):
                ts = calendar.timegm(s["month"].utctimetuple())
            else:
                ts = calendar.timegm(datetime.datetime.strptime(s["month"], "%Y-%m-%d").utctimetuple())
            history.append((
                ts,
                s["trivial"],
                s["robots"],
                s["humans"],
                s["tags"]))

        stats = bmodels.tag_stats()

        check_stats = []
        for check_id, count in bmodels.TodoEntry.stats():
            check = checks.CheckEngine.by_id(check_id)
            check_stats.append((count, check))
        check_stats.sort(reverse=True)

        ctx.update(
            history=json.dumps(history),
            stats=stats,
            checks=check_stats,
        )

        return ctx


class TodoView(DebtagsLayoutMixin, TemplateView):
    template_name = "reports/todo.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        # Top packages with todo entries, sorted by popcon
        ctx["pkgs"] = bmodels.Package.objects.exclude(todolist=None).order_by("-popcon")[:50]
        return ctx


class TodoMaintView(DebtagsLayoutMixin, TemplateView):
    template_name = "reports/maint.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        maint = get_object_or_404(bmodels.Maintainer, email=self.kwargs["email"])
        sources = defaultdict(list)
        for mp in bmodels.MaintPkg.objects.filter(maint=maint).select_related("pkg").order_by("pkg__name"):
            sources[mp.pkg.source].append((mp, bmodels.PkgExtradir(mp.pkg.name)))
        ctx["maint"] = maint
        ctx["sources"] = sorted(sources.items())
        return ctx


class PkgInfo(DebtagsLayoutMixin, TemplateView):
    template_name = "reports/package.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        pkg = bmodels.Package.by_name(self.kwargs["name"])
        if pkg is None:
            raise http.Http404()
        ctx["extra"] = bmodels.PkgExtradir(pkg.name)
        ctx["pkg"] = pkg
        return ctx


class Facets(DebtagsLayoutMixin, TemplateView):
    template_name = "reports/facets.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        facets = bmodels.Facet.objects.all().order_by("name")

        # Compute statistics
        sstats = dict()
        for name, card in bmodels.Tag.card_stats():
            fname = name.split("::")[0]
            sstats[fname] = sstats.get(fname, 0) + card

        ctx["facets"] = [(f, sstats.get(f.name, 0)) for f in facets]
        ctx["sstats"] = sstats

        return ctx


class Facet(DebtagsLayoutMixin, TemplateView):
    template_name = "reports/facet.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)

        facet = bmodels.Facet.by_name(self.kwargs["name"])
        if facet is None:
            raise http.Http404()

        # Compute statistics
        facpfx = facet.name + "::"
        sstats = {name: card for name, card in bmodels.Tag.card_stats() if name.startswith(facpfx)}
        ctx["sstats"] = sstats
        ctx["scount"] = sum(sstats.values())
        ctx["tags"] = [(t, sstats.get(t.name, 0)) for t in facet.tags.order_by("name")]
        ctx["facet"] = facet
        return ctx


class TagInfo(DebtagsLayoutMixin, TemplateView):
    template_name = "reports/tag.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        tag = bmodels.Tag.by_name(self.kwargs["name"])
        if tag is None:
            raise http.Http404()
        ctx["tag"] = tag
        return ctx


class ListChecks(DebtagsLayoutMixin, TemplateView):
    template_name = "reports/list-checks.html"

    def get_context_data(self, **kw):
        from checks import checks
        ctx = super().get_context_data(**kw)

        pkgcount = bmodels.Package.objects.count()
        check_stats = []
        for check_id, count in bmodels.TodoEntry.stats():
            check = checks.CheckEngine.by_id(check_id)
            check_stats.append((count, check))
        check_stats.sort(key=lambda x: x[1].ID)

        ctx["pkgcount"] = pkgcount
        ctx["checks"] = check_stats

        return ctx


class ShowCheck(DebtagsLayoutMixin, TemplateView):
    template_name = "reports/show-check.html"

    def get_context_data(self, **kw):
        from checks import checks
        ctx = super().get_context_data(**kw)

        id = int(self.kwargs["id"])
        check = checks.CheckEngine.by_id(id)
        if check is None:
            raise http.Http404()

        ctx["check"] = check

        # Top packages with todo entries of the given check, sorted by popcon
        ctx["pkgs"] = [pkg for pkg in bmodels.Package.objects.filter(
            todolist__check_id=check.ID).order_by("-popcon")[:50]]

        return ctx


class AuditLog(DebtagsLayoutMixin, TemplateView):
    template_name = "reports/auditlog.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)

        if self.request.user.is_anonymous:
            raise PermissionDenied

        ctx["audit_log"] = self.request.user.audit_log.all().order_by("-ts")

        return ctx


class AprioriRules(DebtagsLayoutMixin, TemplateView):
    template_name = "reports/apriori-rules.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)

        rules = bmodels.load_apriori_ruleset()
        if not rules:
            raise http.Http404()

        ctx["rules"] = rules
        return ctx


class TagsForMaintainer(DebtagsLayoutMixin, View):
    def get(self, request, *args, **kw):
        from backend.utils import add_cors_headers

        # Get list of maintainers to work on
        todo = []
        for k, v in request.GET.items():
            emails = json.loads(v)
            if not isinstance(emails, list):
                continue
            todo.append((k, emails))

        # Count of all packages
        pkg_count = bmodels.Package.objects.count()

        # Global tag cardinalities
        tag_cards = dict(bmodels.Tag.card_stats())

        res = {}
        for key, emails in todo:
            # Parse and complete emails
            maints = []
            for e in emails:
                if "@" in e:
                    addr = e
                else:
                    addr = e + "@debian.org"

                try:
                    m = bmodels.Maintainer.objects.get(email=addr)
                    maints.append(m)
                except bmodels.Maintainer.DoesNotExist:
                    pass

            # From maintainers to packages
            pkgs = bmodels.Package.objects.filter(maintainers__in=maints)

            # From packages to tags
            tcounts = dict()
            for p in pkgs:
                for t in p.stable_tags.all():
                    if t.name in tcounts:
                        tcounts[t.name]["count"] += 1
                    else:
                        tcounts[t.name] = dict(count=1)

            # Add global tag cardinalities and TF/IDF scores
            for k, v in tcounts.items():
                v["tot"] = tag_cards.get(k, 0)
                # v["tfidf"] = "%.2f" % (v["count"] * math.log(pkg_count / v["tot"]))

            res[key] = tcounts

        res = http.HttpResponse(
            json.dumps({
                "pkg_count": pkg_count,
                "maints": res
                }),
            content_type="application/json")
        add_cors_headers(res)
        return res


class Recent(DebtagsLayoutMixin, TemplateView):
    template_name = "reports/recent.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)

        date_from = self.request.GET.get("from", None)
        if date_from is None:
            date_from = datetime.date.today() - datetime.timedelta(days=7)
        else:
            try:
                date_from = datetime.datetime.strptime(date_from, "%Y-%m-%d")
            except ValueError:
                raise http.Http404()
        pkgs = bmodels.Package.objects.order_by("-ts_created")
        pkgs = pkgs.filter(ts_created__gte=date_from)
        pkgs = [pkg.to_dict() for pkg in pkgs]
        ctx["pkgs"] = pkgs
        ctx["date_from"] = date_from
        return ctx
