from __future__ import annotations
from django import http, forms
from django.core.exceptions import PermissionDenied
from django.views.decorators.cache import never_cache
from django.utils.decorators import method_decorator
from django.views.generic.edit import FormView
import json
from io import StringIO
from debtagslayout.mixins import DebtagsLayoutMixin
import backend.models as bmodels
from aptxapianindex.axi import AptXapianIndex
from backend.utils import add_cors_headers
from debdata import patches
import calendar


def serialize_default(obj):
    tt = getattr(obj, "timetuple", None)
    if tt is not None:
        return calendar.timegm(tt())
    else:
        raise TypeError('Object of type %s with value of %s is not JSON serializable' % (type(obj), repr(obj)))


def serialize_to_response(request, data):
    response = http.HttpResponse(content_type="application/json")
    add_cors_headers(response)
    json.dump(data, response, default=serialize_default)
    return response


class PatchForm(forms.Form):
    patch = forms.CharField(widget=forms.Textarea())


class SubmitPatch(DebtagsLayoutMixin, FormView):
    form_class = PatchForm
    template_name = "api/post.html"

    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kw):
        if self.request.user.is_anonymous:
            raise PermissionDenied
        return super(SubmitPatch, self).dispatch(request, *args, **kw)

    def form_valid(self, form):
        """
        Submit a tag patch
        """
        # Parse and validate the patch
        patchset = patches.PatchSet(fd=StringIO(form.cleaned_data["patch"]))

        # Apply the patch, enforcing constraints from anonymous submissions
        db = bmodels.Patches()
        pkgs, checker = db.apply_user_patchset(self.request.user, patchset)

        response = http.HttpResponse(content_type="application/json")
        add_cors_headers(response)
        response.write(json.dumps(dict(
            pkgs=dict(((pkg.name, [x.name for x in pkg.stable_tags.all()]) for pkg in pkgs)),
            notes=checker.remarks,
        )))
        return response


def tag_search_view(request, query):
    """
    Search tags by keyword
    """
    res = []
    if query:
        axi = AptXapianIndex()
        for tag, weight in axi.search_tags(query):
            res.append(dict(n=tag, w=weight))

    response = http.HttpResponse(content_type="application/json")
    add_cors_headers(response)
    response.write(json.dumps(dict(tags=res)))
    return response
