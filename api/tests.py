from __future__ import annotations
from django.test import TestCase, override_settings
from django.urls import reverse
from backend.unittest import BackendDataMixin


@override_settings(BACKEND_DATADIR="testdata")
class TestView(BackendDataMixin, TestCase):
    def test_index(self):
        client = self.make_test_client()
        response = client.get(reverse("api_index"))
        self.assertContains(response, "This page document the site JSON APIs")

    def test_tag_search(self):
        client = self.make_test_client()
        response = client.get(reverse("api_tag_search", args=["package"]))
        self.assertEquals(response.status_code, 200)
        self.assertEqual(response["Content-Type"], "application/json")
        data = response.json()
        tags = data["tags"]
        self.assertEqual({x["n"] for x in tags}, {"suite::debian", "admin::package-management"})

    def test_patch(self):
        client = self.make_test_client()
        response = client.get(reverse("api_patch"))
        self.assertEqual(response.status_code, 403)

        response = client.post(reverse("api_patch"), data={"patch": "debtags: +x11::font"})
        self.assertEqual(response.status_code, 403)

        client = self.make_test_client(self.user)
        response = client.get(reverse("api_patch"))
        self.assertContains(response, "This page allows to post a tag patch manually")

        response = client.post(reverse("api_patch"), data={"patch": "debtags: +x11::font"})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response["Content-Type"], "application/json")
        res = response.json()
        self.assertEqual(res["notes"], [])
        patch = res["pkgs"].pop("debtags")
        self.assertFalse(res["pkgs"])
        self.assertCountEqual(
            patch, [
                'suite::debian', 'x11::font', 'interface::commandline',
                'use::searching', 'implemented-in::c++', 'role::program',
                'scope::utility'])

        response = client.post(reverse("api_patch"), data={"patch": "debtags: +special::not-yet-tagged"})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response["Content-Type"], "application/json")
        res = response.json()
        self.assertEqual(res["notes"], ["debtags: ignoring attempt to add special::* tags"])
        self.assertFalse(res["pkgs"])
