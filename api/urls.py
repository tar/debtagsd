from __future__ import annotations
from django.conf.urls import url
from debtagslayout.views import DebtagsTemplateView
from . import views

urlpatterns = [
    url(r'^$', DebtagsTemplateView.as_view(template_name='api/index.html'), name="api_index"),
    url(r'^patch', views.SubmitPatch.as_view(), name="api_patch"),
    url(r'^tag/search/(?P<query>[^/]+)?$', views.tag_search_view, name="api_tag_search"),
]
